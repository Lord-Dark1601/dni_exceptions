
public class MainDNI {

	public static void main(String[] args) {

		try {
			DNI d2 = new DNI(53786695);
			System.out.println(d2.toString());
			System.out.println(d2.toFormattedString());
		} catch (NIFException nE) {
			System.err.println(nE.getMessage());
		}
		System.out.println();
		try {
			DNI d = new DNI(53786695, 'e');
			System.out.println(d.toString());
			System.out.println(d.toFormattedString());
		} catch (NIFLetterException nE) {
			System.err.println(nE.getMessage());
		}
	}

}
