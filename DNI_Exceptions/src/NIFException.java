
public class NIFException extends Exception {

	public NIFException() {
		super("NIF Exception");
	}

	public NIFException(String msg) {
		super(msg);
	}
}
