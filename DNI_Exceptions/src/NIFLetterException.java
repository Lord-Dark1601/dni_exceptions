
public class NIFLetterException extends NIFException {

	public NIFLetterException() {
		super("NIF Letter Exception");
	}

	public NIFLetterException(String msg) {
		super(msg);
	}
}
